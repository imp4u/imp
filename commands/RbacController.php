<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
    
    public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$admin = $auth->createRole('admin');
		$auth->add($admin);
		
		$manager = $auth->createRole('manager');
		$auth->add($manager);

		$implementor = $auth->createRole('implementor');
		$auth->add($implementor);
	}

	public function actionAp()
	{
		$auth = Yii::$app->authManager;
		

		$createDept = $auth->createPermission('createDept');
		$createDept->description = 'Admin can create new department';
		$auth->add($createDept);

		$createStatus = $auth->createPermission('createStatus');
		$createStatus->description = 'Admin can create new statuses';
		$auth->add($createStatus);

       
	}

	public function actionMp()
	{
		$auth = Yii::$app->authManager;


		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Manager can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Manager can update all users';
		$auth->add($updateUser);

		$deleteUser = $auth->createPermission('deleteUser');
		$deleteUser->description = 'Manager can delete users';
		$auth->add($deleteUser);
		
		$editFix = $auth->createPermission('editFix');
		$editFix->description = 'Manager can edit fix';
		$auth->add($editFix);

		$createStaffType = $auth->createPermission('createStaffType');
		$createStaffType->description = 'Manager can create new Staff Type';
		$auth->add($createStaffType);

		$createTrainingType = $auth->createPermission('createTrainingType');
		$createTrainingType->description = 'Manager can create new Training Type';
		$auth->add($createTrainingType);

		$editStaffType = $auth->createPermission('editStaffType');
		$editStaffType->description = 'Manager can edit Staff Type';
		$auth->add($editStaffType);

		$editTrainingType = $auth->createPermission('editTrainingType');
		$editTrainingType->description = 'Manager can edit Training Type';
		$auth->add($editTrainingType);

		$deleteTrainingType = $auth->createPermission('deleteTrainingType');
		$deleteTrainingType->description = 'Manager can delete Training Type';
		$auth->add($deleteTrainingType);

		$deleteStaffType = $auth->createPermission('deleteStaffType');
		$deleteStaffType->description = 'Manager can delete Staff Type';
		$auth->add($deleteStaffType);

		$viewAdminTools = $auth->createPermission('viewAdminTools');
		$viewAdminTools->description = 'Manager can view admin tools';
		$auth->add($viewAdminTools);
	}


	public function actionIp()
	{
		$auth = Yii::$app->authManager;
		
		$createIssue = $auth->createPermission('createIssue');
		$createIssue->description = 'All Users can create Issue';
		$auth->add($createIssue);

		$editIssue = $auth->createPermission('editIssue');
		$editIssue->description = 'All Users can edit Issue';
		$auth->add($editIssue);

		$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'All Users can update Own User';
		$auth->add($updateOwnUser);

		$createTraining = $auth->createPermission('createTraining');
		$createTraining->description = 'All Users can create Training';
		$auth->add($createTraining);

		$createStaff = $auth->createPermission('createStaff');
		$createStaff->description = 'All Users can create Staff';
		$auth->add($createStaff);

        $editStaff = $auth->createPermission('editStaff');
		$editStaff->description = 'All Users can edit Staff';
		$auth->add($editStaff);

		$editTraining = $auth->createPermission('editTraining');
		$editTraining->description = 'All Users can edit Training';
		$auth->add($editTraining);

		$deleteTraining = $auth->createPermission('deleteTraining');
		$deleteTraining->description = 'All Users can delete Training';
		$auth->add($deleteTraining);

		$deleteStaff = $auth->createPermission('deleteStaff');
		$deleteStaff->description = 'All Users can delete Staff';
		$auth->add($deleteStaff);
	}
	
	

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$implementor = $auth->getRole('implementor');
		$manager = $auth->getRole('manager');
		$admin = $auth->getRole('admin');

		$auth->addChild($admin, $manager);	
		$auth->addChild($admin, $implementor);	
		$auth->addChild($manager, $implementor);	

		// All Users
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->addChild($implementor, $updateOwnUser);

		$createIssue = $auth->getPermission('createIssue');
		$auth->addChild($implementor, $createIssue);

		$createStaff = $auth->getPermission('createStaff');
		$auth->addChild($implementor, $createStaff);

		$createTraining = $auth->getPermission('createTraining');
		$auth->addChild($implementor, $createTraining);

		$editStaff = $auth->getPermission('editStaff');
		$auth->addChild($implementor, $editStaff);

		$deleteStaff = $auth->getPermission('deleteStaff');
		$auth->addChild($implementor, $deleteStaff);


		//Manager
		$createUser = $auth->getPermission('createUser');
		$auth->addChild($manager, $createUser);

		$updateUser = $auth->getPermission('updateUser');
		$auth->addChild($manager, $updateUser);

		$deleteUser = $auth->getPermission('deleteUser');
		$auth->addChild($manager, $deleteUser);
		
		$editFix = $auth->getPermission('editFix');
		$auth->addChild($manager, $editFix);

		$createStaffType = $auth->getPermission('createStaffType');
		$auth->addChild($manager, $createStaffType);

		$editStaffType = $auth->getPermission('editStaffType');
		$auth->addChild($manager, $editStaffType);

		$deleteStaffType = $auth->getPermission('deleteStaffType');
		$auth->addChild($manager, $deleteStaffType);
		
		$createTrainingType = $auth->getPermission('createTrainingType');
		$auth->addChild($manager, $createTrainingType);

		$editTrainingType = $auth->getPermission('editTrainingType');
		$auth->addChild($manager, $editTrainingType);

		$deleteTrainingType = $auth->getPermission('deleteTrainingType');
		$auth->addChild($manager, $deleteTrainingType);

		$viewAdminTools = $auth->getPermission('viewAdminTools');
		$auth->addChild($manager, $viewAdminTools);

		$editIssue = $auth->getPermission('editIssue');
		$auth->addChild($manager, $editIssue);

		$editTraining = $auth->getPermission('editTraining');
		$auth->addChild($manager, $editTraining);

		$deleteTraining = $auth->getPermission('deleteTraining');
		$auth->addChild($manager, $deleteTraining);


		//admin
		$createDept = $auth->getPermission('createDept');
		$auth->addChild($admin, $createDept);

		$createStatus = $auth->getPermission('createStatus');
		$auth->addChild($admin, $createStatus);
		
	}

	public function actionOne()
	{
		$auth = Yii::$app->authManager;
		$implementor = $auth->getRole('implementor');

		$updateOwnIssue = $auth->createPermission('updateOwnIssue');
		$updateOwnIssue->description = 'All Users can update Own Issue';
		$auth->add($updateOwnIssue);

		$updateOwnIssue = $auth->getPermission('updateOwnIssue');
		$auth->addChild($implementor, $updateOwnIssue);
	}

	public function actionTwo()
	{
		$auth = Yii::$app->authManager;
		$implementor = $auth->getRole('implementor');

		$updateOwnTraining = $auth->createPermission('updateOwnTraining');
		$updateOwnTraining->description = 'All Users can update Own Training';
		$auth->add($updateOwnTraining);

		$updateOwnTraining = $auth->getPermission('updateOwnTraining');
		$auth->addChild($implementor, $updateOwnTraining);
	}
}