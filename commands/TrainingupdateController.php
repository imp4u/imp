<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class TrainingupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnTraining = $auth->getPermission('updateOwnTraining');
		$auth->remove($updateOwnTraining);
		
		$rule = new \app\rbac\OwnTrainingRule;
		$auth->add($rule);
				
		$updateOwnTraining->ruleName = $rule->name;		
		$auth->add($updateOwnTraining);	
	}
}