<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class IssueupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnIssue = $auth->getPermission('updateOwnIssue');
		$auth->remove($updateOwnIssue);
		
		$rule = new \app\rbac\OwnIssueRule;
		$auth->add($rule);
				
		$updateOwnIssue->ruleName = $rule->name;		
		$auth->add($updateOwnIssue);	
	}
}