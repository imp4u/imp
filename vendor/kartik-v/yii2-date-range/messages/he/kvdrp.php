<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Apply' => 'חפש',
    'Cancel' => 'ביטול',
    'Custom Range' => 'בחר טווח',
    'From' => 'תאריך התחלה',
    'Last Month' => 'חודש שעבר',
    'Last {n} Days' => '{n} ימים אחרונים',
    'Select Date Range' => 'בחר טווח תאריכים',
    'This Month' => 'חודש נוכחי',
    'To' => 'עד תאריך',
    'Today' => 'היום',
    'W' => 'שבוע',
    'Yesterday' => 'אתמול',
];
