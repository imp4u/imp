<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'משתמשים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <p>
    <?php if (\Yii::$app->user->can('deleteUser')) { ?>
        <?= Html::a('מחק משתמש', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
	<?php } ?>
    <?php if (\Yii::$app->user->can('updateUser') || \Yii::$app->user->can('updateOwnUser', ['user' =>$model]) ){ ?>
        <?= Html::a('עדכן פרטי משתמש', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
         'condensed'=>false,
        'enableEditMode' => false,
        'hover'=>true,
        'mode'=>DetailView::MODE_VIEW,
        'panel'=>[
        'heading'=>'<h2>' . $this->title . '</h2>',
        'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
             [
            'group'=>true,
            'label'=>'פרטים ראשיים',
            'rowOptions'=>['class'=>'success']
            ],
            'id',
            'name',
            'username',
            //'password',
            //'auth_key',
            [
            'group'=>true,
            'label'=>'פרטי עריכה',
            'rowOptions'=>['class'=>'success']
            ],
            ['columns' => [
            [
                //Created at of the user
                'label' => $model->attributeLabels()['created_at'],
                'value' => date('Y-m-d H:i A', $model->created_at),
                'valueColOptions'=>['class'=>'col-md-4'],
            ],
            [
                //Updated at of the user
                'label' => $model->attributeLabels()['updated_at'],
                'value' => date('Y-m-d H:i A', $model->updated_at),
                'valueColOptions'=>['class'=>'col-md-4'],
            ],
            ]],
            ['columns' => [
            [ // Who Created the User
				'label' => $model->attributeLabels()['created_by'],
				'format' => 'html',
                'valueColOptions'=>['class'=>'col-md-4'],
                'value' => 
                    is_object($model->createdBy) ? (Html::a($model->createdBy->name, ['user/view', 'id' => $model->createdBy->id])) : "לא הוגדר",
			],
            [ // Who updated the User
				'label' => $model->attributeLabels()['updated_by'],
				'format' => 'html',
                'valueColOptions'=>['class'=>'col-md-4'],
				'value' => 
                    is_object($model->updatedBy) ? (Html::a($model->updatedBy->name, ['user/view', 'id' => $model->updatedBy->id])) : "לא הוגדר",
			],
            ]],
            
        ],
    ]) ?>

</div>
