<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'משתמשים';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            'username',
            //'password',
            //'auth_key',
             //'created_by',
             /*[
				'attribute' => 'created_by',
				'label' => 'נוצר על ידי',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->findIdentity($model->id)->name, ['user/view', 'id' => $model->findIdentity($model->id)->id]);
				},
			], */
             [
                'attribute' => 'created_at',
                'value' => function($model){return date('d/m/Y H:i', $model->created_at);
				},
				'label' => 'נוצר בתאריך'
            ],
             [
				'attribute' => 'role',
				'label' => 'סוג משתמש',
				'format' => 'raw',
				'value' => function ($model) {
					return array_keys(\Yii::$app->authManager->getRolesByUser($model->id))[0];
				},
				'filter'=>Html::dropDownList('UserSearch[role]', $role, $roles, ['class'=>'form-control']),
			],
            //'userole:text:סוג משתמש',

             ['class' => 'yii\grid\ActionColumn',
			'visibleButtons' => [
			'delete' => (\Yii::$app->user->can('deleteUser')),
			],
			],
        ];
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (\Yii::$app->user->can('createUser')) { ?>
    <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <p>
        <?= Html::a('צור משתמש חדש', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>
    <div class="pull-left">
    <?php
    echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'exportConfig' => [
        'PDF' => false,
        'HTML' => false,
        'CSV' => false,
    ],
    'fontAwesome' => true,
    'dropdownOptions' => [
        'label' => 'ייצא נתונים',
        'class' => 'btn btn-default'
    ]
    ]);
    //echo "<hr>\n";

    ?>
    </div>
</div>
	<?php } ?>

   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>
