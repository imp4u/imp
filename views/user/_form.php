<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <div class="col-md-4">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= ($model->isNewRecord || (\Yii::$app->user->can('createUser'))) ? $form->field($model, 'username')->textInput(['maxlength' => true]) : "" ?>

    <?= ($model->isNewRecord || (\Yii::$app->user->can('updateOwnUser', ['user' =>$model]))) ? $form->field($model, 'password')->passwordInput(['maxlength' => true]) : ""?>

    <?php if (\Yii::$app->user->can('createUser')) { ?>
         <?=
                $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' => $roles,
                    'options' => ['placeholder' => 'בחר תפקיד'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>		
	<?php } ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור משתמש' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
