<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'מחלקות';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (\Yii::$app->user->can('createDept')) { ?>
    <p>
        <?= Html::a('הוספת מחלקה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',

            ['class' => 'yii\grid\ActionColumn',
			'visibleButtons' => [
			'delete' => (\Yii::$app->user->can('createDept')),
            'update' => (\Yii::$app->user->can('createDept')),
			],
			],
        ],
    ]); ?>
</div>
