<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'מחלקות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (\Yii::$app->user->can('createDept')) { ?>
        <p>
            <?= Html::a('עדכן', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('מחק', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php } ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
               // 'id',
                'name',
            ],
        ]) ?>

</div>
