<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use app\controllers\StaffController;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'אנשי צוות רפואי', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-view">
    <p>
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'למחוק חבר צוות?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'condensed'=>false,
        'enableEditMode' => false,
        'hover'=>true,
        'mode'=>DetailView::MODE_VIEW,
        'panel'=>[
        'heading'=>'<h2> פרטי צוות רפואי: ' . $this->title . '</h2>',
        'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'id',
            'name',
            [
                //show staff_type
                'label' => $model->attributeLabels()['staff_type'],
                'value' => $model->staffType->name,
            ],
            [
                //show department
                'label' => $model->attributeLabels()['department'],
                'value' => $model->staffDepartment->name,
            ],
        ],
    ]) ?>
    
    <div class="jumbotron">
    <h3>הדרכות שבוצעו ל<?=$model->name ?></h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'date',
                'value' => function($model){return date('d/m/Y', $model->date);
				},
				'label' => 'תאריך',
				'format' => 'raw',
            ],
            'shift',
            [
				'attribute' => 'imp_id',
				'label' => 'שם המטמיע',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->trainingImplementor->name, ['user/view', 'id' => $model->trainingImplementor->id]);
				},
			],
            [
				'attribute' => 'training_type',
				'label' => 'סוג הדרכה',
				'format' => 'raw',
				'value' => function($model){
					return $model->trainingType->name;
				},
			],
            'notes',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
