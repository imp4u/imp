<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\StaffType;
use app\models\Department;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?=
                $form->field($model, 'staff_type')->widget(Select2::classname(), [
                    'data' => StaffType::getStaffTypes(),
                    'options' => ['placeholder' => 'בחר תפקיד'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>
    <?=
                $form->field($model, 'department')->widget(Select2::classname(), [
                    'data' => Department::getDepartments(),
                    'options' => ['placeholder' => 'בחר מחלקה'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור איש צוות' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
