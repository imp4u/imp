<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;
use kartik\datecontrol\DateControl;
use kartik\datetime\DateTimePicker;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel app\models\IssueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'תקלות';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns=[
            'id',
            'shift',
            [
                'attribute' => 'date',
                'value' => function($model){return date('d/m/Y', $model->date);},
				'label' => 'תאריך',
				'format' => 'raw',
                'filter' => DateRangePicker::widget([
                    'name'=>'date_range',
                    'presetDropdown'=>true,
                    'hideInput'=>true,
                    //'convertFormat'=>true,
                    'pluginOptions' => [
                        'locale'=>[
                            'separator'=>'to',
                        ],
                        'opens'=>'left',
                    ]
                    
                ])
            ],
            [
				'attribute' => 'created_by',
				'label' => 'שם המטמיע',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->issueImplementor->name, ['user/view', 'id' => $model->issueImplementor->id]);
				},
				'filter'=>Html::dropDownList('IssueSearch[created_by]', $implementor, $implementors, ['class'=>'form-control']),
			],
            [
				'attribute' => 'staff_name',
				'label' => 'שם איש צוות רפואי',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->issueStaff->name, ['staff/view', 'id' => $model->issueStaff->id]);
				},
			],
            ['attribute' => 'description',
        'format' => 'ntext'],
            [
				'attribute' => 'status',
				'label' => 'סטאטוס',
				'format' => 'raw',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('IssueSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
            //'assign_to',
            //'fix_description',
            //'notes',
            [
				'attribute' => 'department',
				'label' => 'מחלקה',
				'format' => 'raw',
				'value' => function($model){
					return $model->issueDepartment->name;
				},
				'filter'=>Html::dropDownList('IssueSearch[department]', $department, $departments, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ];
?>
<div class="issue-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <p>
        <?= Html::a('הדרכה חדשה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    <div class="pull-left">
    <?php
    echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'exportConfig' => [
        'PDF' => false,
        'HTML' => false,
        'CSV' => false,
    ],
    'fontAwesome' => true,
    'dropdownOptions' => [
        'label' => 'ייצא נתונים',
        'class' => 'btn btn-default'
    ]
    ]);
    //echo "<hr>\n";

    ?>
    </div>
</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pager' => [
        'firstPageLabel' => 'First',
        'lastPageLabel'  => 'Last'],
    ]); ?>
</div>
