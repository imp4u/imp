<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Issue */

$this->title = 'עדכון תקלה - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'תקלות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'תקלה מספר - ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="issue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
