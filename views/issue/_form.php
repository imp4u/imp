<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\datecontrol\DateControl;
use kartik\datetime\DateTimePicker;
use app\models\Status;
use app\models\User;
use app\models\Issue;
use app\models\Department;
use app\models\Staff;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Issue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issue-form">
 <?php $form = ActiveForm::begin();

  ?>
    <div class="row pull-right">
        <div class="col-md-6">
                <?=
                $form->field($model, 'department')->widget(Select2::classname(), [
                    'data' => Department::getDepartments(),
                    'options' => ['placeholder' => 'בחר מחלקה'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                        'type'=>DateControl::FORMAT_DATE,
                        'ajaxConversion'=>true,
                        'saveFormat' => 'php:U',
                        'options' => [
                            'pluginOptions' => [
                                'autoclose' => true
                            ]
                        ]
                    ]);
                ?>
                
                <?=
                $form->field($model, 'shift')->widget(Select2::classname(), [
                    'data' => Issue::getShifts(),
                    'options' => ['placeholder' => 'בחר משמרת'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?=
                    $form->field($model, 'staff_name')->widget(Select2::classname(), [
                        'data' => Staff::getStaffMembers(),
                        'options' => ['placeholder' => 'בחר איש צוות'],
                        'language' => 'he',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                ?>

                <?= $form->field($model, 'admission_id')->textInput() ?>
        </div>
        <div class="col-md-6">
                <?= $form->field($model, 'description')->textArea(['rows' => '7']) ?>

                <?=
                    $form->field($model, 'status')->widget(Select2::classname(), [
                        'data' => Status::getStatuses(),
                        //'options' => ['placeholder' => 'בחר סטאטוס'],
                        'language' => 'he',
                        'pluginOptions' => [
                            'allowClear' => true,
                            'initValueText' => '1',
                        ],
                    ]);
                ?>
                <?php if (\Yii::$app->user->can('editFix')) { 
                echo $form->field($model, 'assign_to')->textInput();

                echo $form->field($model, 'fix_description')->textInput(['maxlength' => true]);

                echo $form->field($model, 'notes')->textInput(['maxlength' => true]);
                } ?>
        </div>
    </div>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'פתח תקלה' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
