<?php

use yii\helpers\Html;
// use yii\widgets\DetailView;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Issue */

$this->title = "תקלה מספר  " . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'תקלות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issue-view">

    <p>
        <?= Html::a('עדכון פרטי תקלה', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחק תקלה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'למחוק את התקלה?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'condensed'=>false,
        'enableEditMode' => false,
        'hover'=>true,
        'mode'=>DetailView::MODE_VIEW,
        'panel'=>[
        'heading'=>'<h2>' . $this->title . '</h2>',
        'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            [
            'group'=>true,
            'label'=>'פרטי התקלה',
            'rowOptions'=>['class'=>'success']
            ],
            ['columns' => [
                        [
                        //Date of issue
                            'label' => $model->attributeLabels()['date'],
                            'value' => date('d/m/Y H:i', $model->date),
                            'valueColOptions'=>['class'=>'col-md-2'],
                        ],
                        ['attribute'=> 'shift',
                        'valueColOptions'=>['class'=>'col-md-2']],

                        [ // Who Created the User
                            'label' => $model->attributeLabels()['created_by'],
                            'format' => 'html',
                            'valueColOptions'=>['class'=>'col-md-3'],
                            'value' => 
                                is_object($model->issueImplementor) ? (Html::a($model->issueImplementor->name, ['user/view', 'id' => $model->issueImplementor->id])) : "לא הוגדר",
                        ],

                        ]
                ],
            ['columns' => [
                        ['attribute'=> 'staff_name',
                        'format' => 'html',
                        'value' => is_object($model->issueStaff) ? (Html::a($model->issueStaff->name, ['staff/view', 'id' => $model->issueStaff->id])) : "לא הוגדר",
                        'valueColOptions'=>['class'=>'col-md-2']],
                        ['attribute'=> 'admission_id',
                        'valueColOptions'=>['class'=>'col-md-2']],
                        [
                            //show department
                            'label' => $model->attributeLabels()['department'],
                            'value' => $model->issueDepartment->name,
                            'valueColOptions'=>['class'=>'col-md-3']
                            
                        ],
            ]
            ],
        ['attribute' => 'description',
        'format' => 'ntext'],
            [
            'group'=>true,
            'label'=>'פרטי הטיפול',
            'rowOptions'=>['class'=>'success']
            ],
        [
                //show status
                'label' => $model->attributeLabels()['status'],
                'value' => $model->statusItem->name,
        ],
            'assign_to',
            'fix_description',
            'notes',

        [
            'group'=>true,
            'label'=>'פרטי עדכון',
            'rowOptions'=>['class'=>'success']
        ],
        ['columns' => [
            [ // Who updated the issue
                            'label' => $model->attributeLabels()['updated_by'],
                            'format' => 'html',
                            'valueColOptions'=>['class'=>'col-md-3'],
                            'value' => 
                                is_object($model->updatedBy) ? (Html::a($model->updatedBy->name, ['user/view', 'id' => $model->updatedBy->id])) : "לא עודכן",
            ],
            [
            //Date of update
                            'label' => $model->attributeLabels()['updated_at'],
                            'value' => ($model->updated_at == NULL) ? "לא עודכן" : date('d/m/Y', $model->updated_at),
                            'valueColOptions'=>['class'=>'col-md-2'],
            ],
            ]
        ], 

        ]
    ]) ?>


</div>
