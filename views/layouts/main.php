<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\nav\NavX;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="he" dir="rtl">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo3.png', ['alt'=>Yii::$app->name, 'class'=> 'img-responsive']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    echo NavX::widget([
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [
            //['label' => '<i class="fa fa-home" aria-hidden="true"></i> בית ', 'url' => ['/site/index']],
            Yii::$app->user->isGuest ? "" : (
            ['label' => '<i class="fa fa-users" aria-hidden="true"></i> משתמשים', 'url' => ['/users']]),
            Yii::$app->user->isGuest ? "" : (
            ['label' => '<i class="fa fa-bug" aria-hidden="true"></i> תקלות', 'url' => ['/issues']]),
            Yii::$app->user->isGuest ? "" : (
            ['label' => '<i class="fa fa-stethoscope" aria-hidden="true"></i> אנשי צוות רפואי', 'url' => ['/staff']]),
            Yii::$app->user->isGuest ? "" : (
            ['label' => '<i class="fa fa-hand-o-up" aria-hidden="true"></i> מעקב הדרכות', 'url' => ['/training']]),
            
            Yii::$app->user->can('viewAdminTools') ?
            (
                    ['label' => '<i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i> כלי ניהול', 'items' => [
                    Yii::$app->user->can('createDept') ? (
                    ['label' => 'עריכת מחלקות', 'url' => ['/dept']]) : "",
                    Yii::$app->user->can('createTrainingType') ? (
                    ['label' => 'עריכת סוגי הדרכה', 'url' => ['/training-type']]) : "",
                    Yii::$app->user->can('createStaffType') ? (
                    ['label' => 'עריכת סוגי אנשי צוות', 'url' => ['/staff-type']]) : "",
                    Yii::$app->user->can('createStatus') ? (
                    ['label' => 'עריכת סוגי סטטוסים', 'url' => ['/status']]) : "",
                    ],
                    ]
            ) : ""
            ,


            Yii::$app->user->isGuest ? (
                ['label' => '<i class="fa fa-sign-in" aria-hidden="true"></i> התחברות', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    '<i class="fa fa-sign-out" aria-hidden="true"></i> התנתק (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; IMP - טל בראשי, דויד ענטבי ויונתן דרור <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
