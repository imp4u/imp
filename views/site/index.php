<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'IMP - Implementing Tracker';
?>
<div class="site-index">

    <div class="jumbotron">
        
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-lg-offset-4 col-lg-4">
            <?= Html::img('@web/images/logo.png', ['alt'=>Yii::$app->name, 'class'=> 'img-responsive']) ?>
            </div>
        </div>
        
        

        <?php if(!Yii::$app->user->isGuest) { ?>
        <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> פתיחת תקלה חדשה', ['issue/create'], ['class' => 'btn btn-success']) ?>
        <?php } ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>מעקב אחר תקלות</h2>

                <p>המערכת תעזור לעקוב אחר תקלות שונות שהמטמיעים נתקלים במהלך המשמרת שלהם ובכך תעזור לוודא שהתקלה אכן נפתרה וע״י מי היא נפתרה.
                </p>

                <?= Html::a('תקלות', ['issue/index'], ['class' => 'btn btn-info']) ?>
            </div>
            <div class="col-lg-6">
                <h2>מעקב אחר הדרכות</h2>

                <p>ל איש צוות של בית החולים (אחות, רופא, מתמחה) יעבור הדרכה והמערכת תשמור את כל הנתונים לגבי כל איש צוות, הדבר יעזור למטמיעים לדעת מי זקוק להדרכה נוספת ובאילו נושאים.</p>

                <?= Html::a('הדרכות', ['training/index'], ['class' => 'btn btn-info']) ?>
            </div>
            
        </div>

    </div>
</div>
