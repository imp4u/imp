<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Training */

$this->title = 'עדכון פרטי הדרכה - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'הדרכות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'הדרכה מספר - ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'עדכון';
?>
<div class="training-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
