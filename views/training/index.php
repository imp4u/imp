<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\datecontrol\DateControl;
use kartik\daterange\DateRangePicker;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'הדרכות';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'date',
                'value' => function($model){return date('d/m/Y', $model->date);},
				'label' => 'תאריך',
				'format' => 'raw',
                'filter' => DateRangePicker::widget([
                    'name'=>'date_range',
                    'presetDropdown'=>true,
                    'hideInput'=>true,
                    //'convertFormat'=>true,
                    'pluginOptions' => [
                        'locale'=>[
                            'separator'=>'to',
                        ],
                        'opens'=>'left',
                    ]
                    
                ])
            ],
            'shift',
            [
				'attribute' => 'created_by',
				'label' => 'שם המטמיע',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->trainingImplementor->name, ['user/view', 'id' => $model->trainingImplementor->id]);
				},
                'filter'=>Html::dropDownList('TrainingSearch[created_by]', $implementor, $implementors, ['class'=>'form-control']),
			],
            [
				'attribute' => 'staff_id',
				'label' => 'שם איש הצוות',
				'format' => 'raw',
				'value' => function($model){return Html::a($model->trainingStaff->name, ['staff/view', 'id' => $model->trainingStaff->id]);
				},
			],
            [
				'attribute' => 'training_type',
				'label' => 'סוג הדרכה',
				'format' => 'raw',
				'value' => function($model){
					return $model->trainingType->name;
				},
			],
            ['attribute' => 'notes',
        'format' => 'ntext'],

            ['class' => 'yii\grid\ActionColumn'],
        ];
?>
<div class="training-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <p>
        <?= Html::a('הדרכה חדשה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    <div class="pull-left">
    <?php
    echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'exportConfig' => [
        'PDF' => false,
        'HTML' => false,
        'CSV' => false,
    ],
    'fontAwesome' => true,
    'dropdownOptions' => [
        'label' => 'ייצא נתונים',
        'class' => 'btn btn-default'
    ]
    ]);
    //echo "<hr>\n";

    ?>
    </div>
</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pager' => [
        'firstPageLabel' => 'First',
        'lastPageLabel'  => 'Last'],
    ]); ?>
</div>
