<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use kartik\datetime\DateTimePicker;
use app\models\Status;
use app\models\User;
use app\models\Issue;
use app\models\TrainingType;
use app\models\Department;
use app\models\Staff;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Training */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                        'type'=>DateControl::FORMAT_DATE,
                        'ajaxConversion'=>true,
                        'saveFormat' => 'php:U',
                        'options' => [
                            'pluginOptions' => [
                                'autoclose' => true
                            ]
                        ]
                    ]);
                ?>

    <?=$form->field($model, 'shift')->widget(Select2::classname(), [
                    'data' => Issue::getShifts(),
                    'options' => ['placeholder' => 'בחר משמרת'],
                    'language' => 'he',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
    ?>

    <?=$form->field($model, 'staff_id')->widget(Select2::classname(), [
                        'data' => Staff::getStaffMembers(),
                        'options' => ['placeholder' => 'בחר איש צוות'],
                        'language' => 'he',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
    ?>

    

    <?=$form->field($model, 'training_type')->widget(Select2::classname(), [
                        'data' => TrainingType::getTrainingTypes(),
                        'options' => ['placeholder' => 'בחר סוג הדרכה'],
                        'language' => 'he',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
    ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור הדרכה' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
