<?php

use yii\helpers\Html;
// use yii\widgets\DetailView;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Training */

$this->title = "הדרכה מספר  " . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'הדרכות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-view">

    

    <p>
        <?= Html::a('עדכון', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'למחוק את ההדרכה?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'condensed'=>false,
        'enableEditMode' => false,
        'hover'=>true,
        'mode'=>DetailView::MODE_VIEW,
        'panel'=>[
        'heading'=>'<h2>' . $this->title . '</h2>',
        'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            [
            'group'=>true,
            'label'=>'פרטי ההדרכה',
            'rowOptions'=>['class'=>'success']
            ],
            ['columns' => [
                        [
                        //Date of training
                            'label' => $model->attributeLabels()['date'],
                            'value' => date('d/m/Y', $model->date),
                            'valueColOptions'=>['class'=>'col-md-2'],
                        ],
                        ['attribute'=> 'shift',
                        'valueColOptions'=>['class'=>'col-md-2']],

                        [ // Who Created the training
                            'label' => $model->attributeLabels()['created_by'],
                            'format' => 'html',
                            'valueColOptions'=>['class'=>'col-md-3'],
                            'value' => is_object($model->trainingImplementor) ? (Html::a($model->trainingImplementor->name, ['user/view', 'id' => $model->trainingImplementor->id])) : "לא הוגדר",
                            ],
                        ]
            ],
            ['columns' => [
                        ['attribute'=> 'staff_id',
                        'format' => 'html',
                        'value' => is_object($model->trainingStaff) ? (Html::a($model->trainingStaff->name, ['staff/view', 'id' => $model->trainingStaff->id])) : "לא הוגדר",
                        'valueColOptions'=>['class'=>'col-md-2']],
                        [ 
                        'label' => $model->attributeLabels()['training_type'],
                        'value' => $model->trainingType->name,
                        'valueColOptions'=>['class'=>'col-md-6']],          
            ]
            ],
            ['attribute' => 'notes',
            'format' => 'ntext'],
            [
            'group'=>true,
            'label'=>'פרטי עדכון',
            'rowOptions'=>['class'=>'success']
        ],
        ['columns' => [
            [ // Who updated the issue
                            'label' => $model->attributeLabels()['updated_by'],
                            'format' => 'html',
                            'valueColOptions'=>['class'=>'col-md-3'],
                            'value' => 
                                is_object($model->updatedBy) ? (Html::a($model->updatedBy->name, ['user/view', 'id' => $model->updatedBy->id])) : "לא עודכן",
            ],
            [
            //Date of update
                            'label' => $model->attributeLabels()['updated_at'],
                            'value' => ($model->updated_at == NULL) ? "לא עודכן" : date('d/m/Y', $model->updated_at),
                            'valueColOptions'=>['class'=>'col-md-2'],
            ],
            ]
        ], 
        ],
    ]) ?>

</div>
