<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingType */

$this->title = 'יצירת סוג הדרכה';
$this->params['breadcrumbs'][] = ['label' => 'סוגי הדרכות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
