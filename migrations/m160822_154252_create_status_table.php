<?php

use yii\db\Migration;

/**
 * Handles the creation for table `status`.
 */
class m160822_154252_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'name' => 'string',
        ]);

        $this->insert('status',['name' => 'ראשוני']);
        $this->insert('status',['name' => 'בטיפול']);
        $this->insert('status',['name' => 'טופל']);
        $this->insert('status',['name' => 'בבדיקה']);
        $this->insert('status',['name' => 'לדיון']);
        $this->insert('status',['name' => 'לא באג']);
        $this->insert('status',['name' => 'לטיפול עתידי']);
        $this->insert('status',['name' => 'לא משתחזר']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
