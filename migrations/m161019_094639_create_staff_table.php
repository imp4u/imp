<?php

use yii\db\Migration;

/**
 * Handles the creation for table `staff`.
 */
class m161019_094639_create_staff_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('staff', [
            'id' => $this->primaryKey(),
            'name' => 'string',
            'staff_type' => 'integer',
            'department' => 'integer',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('staff');
    }
}
