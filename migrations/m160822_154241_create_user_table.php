<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160822_154241_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => 'string',
            'username' => 'string',
            'password' => 'string',
            'auth_key' => 'string',
            'updated_by'=>'integer',
            'updated_at'=>'integer',
            'created_by'=>'integer',
            'created_at'=>'integer',
        ]);
		
		 $this->insert('user',['name' => 'impadmin',
							   'username' => 'impadmin',
							   'password' => 'impadmin',
							   ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
