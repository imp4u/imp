<?php

use yii\db\Migration;

/**
 * Handles the creation for table `staff_type`.
 */
class m161019_094712_create_staff_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('staff_type', [
            'id' => $this->primaryKey(),
            'name' => 'string',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('staff_type');
    }
}
