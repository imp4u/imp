<?php

use yii\db\Migration;

/**
 * Handles the creation for table `training`.
 */
class m161019_094736_create_training_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('training', [
            'id' => $this->primaryKey(),
            'date' => 'integer',
            'shift' => 'string',
            'staff_id' => 'integer',
            'created_by'=>'integer',
            'updated_by'=>'integer',
            'created_at'=>'integer',
            'updated_at'=>'integer',
            'training_type' => 'integer',
            'notes' => 'string',
           
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('training');
    }
}
