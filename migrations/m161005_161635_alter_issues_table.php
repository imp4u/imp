<?php

use yii\db\Migration;

class m161005_161635_alter_issues_table extends Migration
{
    public function up()
    {
        $this->addColumn('issue','department','integer');
    }

    public function down()
    {
        $this->dropColumn('issue','department');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
