<?php

use yii\db\Migration;

/**
 * Handles the creation for table `training_type`.
 */
class m161019_094745_create_training_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('training_type', [
            'id' => $this->primaryKey(),
            'name' => 'string',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('training_type');
    }
}
