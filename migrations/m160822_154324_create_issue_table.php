<?php

use yii\db\Migration;

/**
 * Handles the creation for table `issue`.
 */
class m160822_154324_create_issue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('issue', [
            'id' => $this->primaryKey(),
            'date' => 'integer',
            'shift' => 'string',
            'created_by'=>'integer',
            'updated_by'=>'integer',
            'created_at'=>'integer',
            'updated_at'=>'integer',
            'staff_name' => 'string',
            'admission_id' => 'integer',
            'description' => 'string',
            'status' => 'integer',
            'assign_to' => 'string',
            'fix_description' => 'string',
            'notes' => 'string',
        ]);

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('issue');
    }
}
