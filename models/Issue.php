<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "issue".
 *
 * @property integer $id
 * @property integer $date
 * @property string $shift
 * @property integer $created_by
 * @property string $staff_name
 * @property integer $admission_id
 * @property string $description
 * @property integer $status
 * @property integer $assign_to
 * @property string $fix_description
 * @property string $notes
 * @property string $department
 */
class Issue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admission_id','department', 'status','updated_by', 'updated_at', 'created_by', 'created_at'], 'integer'],
            [['date'], 'safe'],
            [['shift', 'staff_name', 'description', 'fix_description', 'notes', 'assign_to'], 'string', 'max' => 255],
            [['department', 'status', 'date', 'shift', 'description', 'staff_name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'מספר תקלה',
            'date' => 'זמן התקלה',
            'shift' => 'משמרת',
            'staff_name' => 'רופא/אחות',
            'admission_id' => 'מספר אשפוז',
            'description' => 'תיאור תקלה',
            'status' => 'סטאטוס',
            'assign_to' => 'בטיפול ע״י',
            'fix_description' => 'תיאור הטיפול',
            'notes' => 'הערה',
            'department' => 'מחלקה',
            'updated_by' => 'עודכן על ידי',
            'updated_at' => 'עודכן בתאריך',
            'created_by' => 'נוצר על ידי',
            'created_at' => 'נוצר בתאריך',
        ];
    }

    public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

    	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    	public function getIssueImplementor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy(){
        return $this->hasOne(User::className(), ['id'=>'updated_by']);
    }

        public function getIssueDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }


    public static function getShifts()
    {
        $shifts=[

                    'בוקר' => 'בוקר',
                    'ערב' => 'ערב',
                    'לילה' => 'לילה',
                ];
        return $shifts;
    }
       public function getIssueStaff()
    {
        return $this->hasOne(Staff::className(), ['id' => 'staff_name']);

    }

    public static function findIssueFromUser($userId)
    {
        return static::find()
		->where(['created_by'=>$userId])
		->one();
    }
}
