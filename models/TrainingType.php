<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "training_type".
 *
 * @property integer $id
 * @property string $name
 */
class TrainingType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'סוג הדרכה',
        ];
    }

    public static function getTrainingTypes()
	{
		$trainingTypes = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $trainingTypes;						
	}
}
