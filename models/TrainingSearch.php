<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Training;

/**
 * TrainingSearch represents the model behind the search form about `app\models\Training`.
 */
class TrainingSearch extends Training
{
     public $date_range;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date', 'created_by'], 'integer'],
            [['shift', 'notes', 'staff_id', 'training_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Training::find();
        $query -> joinWith(['trainingImplementor', 'trainingStaff', 'trainingType']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $this->created_by == -1 ? $this->created_by = null : $this->created_by;
        if(!empty($_GET['date_range'])){
        $this->date_range = $_GET['date_range'];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'training.id' => $this->id,
            'training.created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'shift', $this->shift]);
        $query->andFilterWhere(['like', 'notes', $this->notes]);
        $query->andFilterWhere(['like', 'staff.name', $this->staff_id]);
        $query->andFilterWhere(['like', 'training_type.name', $this->training_type]);

        if(!empty($this->date_range) && strpos($this->date_range, 'to') !== false) {
			list($start_date, $end_date) = explode('to', $this->date_range);
			$query->andFilterWhere(['between', 'date', strtotime($start_date), strtotime($end_date)]);
		}	

        return $dataProvider;
    }
}
