<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
    public static function getStatuses()
	{
		$statuses = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $statuses;						
	}
	
	public static function getStatusesWithAllStatuses()
	{
		$allStatuses = self::getStatuses();
		$allStatuses[-1] = 'כל הסטאטוסים';
		$allStatuses = array_reverse ( $allStatuses, true );
		return $allStatuses;	
	}
}
