<?php

namespace app\models;

use Yii;
use \yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property integer $updated_by
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $created_at
 */

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
	
	public $role; 
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_by', 'updated_at', 'created_by', 'created_at'], 'integer'],
            [['name', 'username', 'password', 'auth_key'], 'string', 'max' => 255],
            ['role', 'safe'],
			[['role', 'name', 'username', 'password'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'שם מלא',
            'username' => 'שם משתמש',
            'password' => 'סיסמא',
            'auth_key' => 'Auth Key',
            'updated_by' => 'עודכן על ידי',
            'updated_at' => 'עודכן בתאריך',
            'created_by' => 'נוצר על ידי',
            'created_at' => 'נוצר בתאריך',
			'role' => 'תפקיד',
        ];
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getName()
    {
        return $this->name;
    }

	
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $users;						
	}
	
	public static function getUsersWithAllUsers()
	{
		$users = self::getUsers();
		$users[-1] = 'כל המשתמשים';
		$users = array_reverse ( $users, true );
		return $users;	
	}

	public static function getUsersWithIssues()
	{
		$users = self::getUsers();

		foreach ($users as $id => $user) {
			if (Issue::findIssueFromUser($id)==NULL) {
				unset($users[$id]);
			}
		}
		$users[-1] = 'כל המשתמשים';
		$users = array_reverse ( $users, true );
		return $users;	
	}

	public static function getUsersWithTraining()
	{
		$users = self::getUsers();

		foreach ($users as $id => $user) {
			if (Training::findTrainFromUser($id)==NULL) {
				unset($users[$id]);
			}
		}
		$users[-1] = 'כל המשתמשים';
		$users = array_reverse ( $users, true );
		return $users;	
	}

	
	public static function findIdentity($id)
    {
        return static::findOne($id);
    }
	
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	public static function findIdentityByAccessToken($token, $type = null)
    {
       
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }
	
	public function getId()
    {
        return $this->id;
    }

    public function getCreatedBy(){
        return $this->hasOne(self::className(), ['id'=>'created_by']);
    }
    public function getUpdatedBy(){
        return $this->hasOne(self::className(), ['id'=>'updated_by']);
    }
	
	public function getAuthKey()
     {
        return $this->auth_key;
     }
	 
	 public function validateAuthKey($authKey)
     {
         return $this->getAuthKey() === $authKey;
     }	
	 
	 public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
    public function getUserole()
    {
		$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
	
	public static function getRoleWithAllRoles()
	{
		$roles=self::getRoles();
		$roles[-1] = 'כל הסוגים';
		$roles = array_reverse ( $roles, true );
		return $roles;
	}
 

    public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}

	public static function getImplementors()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $users;						
	}

	
	 public function beforeSave($insert)
     {
        $return = parent::beforeSave($insert);

       if ($this->isAttributeChanged('password'))
           $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

       if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

       return $return;
     }

     public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
        if (!\Yii::$app->user->can('updateUser')){
			return $return;
		}
		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){ //new user
			$auth->assign($role, $this->id);
		} else { //updated user
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }	
}
