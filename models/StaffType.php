<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "staff_type".
 *
 * @property integer $id
 * @property string $name
 */
class StaffType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'שם התפקיד',
        ];
    }

    public static function getStaffTypes()
	{
		$staffTypes = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $staffTypes;						
	}

    public static function findTypeByName($name)
	{
		return static::findOne(['name' => $name]);				
	}

    public function getId(){
        return $this->id;
    }
}
