<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $name
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'קוד מחלקה',
            'name' => 'שם מחלקה',
        ];
    }
	
	public static function getDepartments()
	{
		$allDepartments = self::find()->all();
		$allDepartmentsArray = ArrayHelper::
					map($allDepartments, 'id', 'name');
		return $allDepartmentsArray;						
	}
	
	public static function getDepartmentsWithAllDepartments()
	{
		$allDepartments = self::getDepartments();
		$allDepartments[-1] = 'כל המחלקות';
		$allDepartments = array_reverse ( $allDepartments, true );
		return $allDepartments;	
	}
}
