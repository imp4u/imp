<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use \yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "training".
 *
 * @property integer $id
 * @property integer $date
 * @property string $shift
 * @property integer $staff_id
 * @property integer $imp_id
 * @property integer $training_type
 * @property string $notes
 */
class Training extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_by', 'updated_at', 'created_by', 'created_at', 'date', 'staff_id', 'training_type'], 'integer'],
            [['shift', 'notes'], 'string', 'max' => 255],
            [['date', 'staff_id','shift', 'training_type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'תאריך',
            'shift' => 'משמרת',
            'staff_id' => 'איש צוות רפואי',
            'training_type' => 'סוג הדרכה',
            'notes' => 'הערות',
            'updated_by' => 'עודכן על ידי',
            'updated_at' => 'עודכן בתאריך',
            'created_by' => 'נוצר על ידי',
            'created_at' => 'נוצר בתאריך',
        ];
    }

    public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

    public function getTrainingImplementor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getTrainingStaff()
    {
        return $this->hasOne(Staff::className(), ['id' => 'staff_id']);
    }

    public function getTrainingType()
    {
        return $this->hasOne(TrainingType::className(), ['id' => 'training_type']);
    }

    public function getUpdatedBy(){
        return $this->hasOne(User::className(), ['id'=>'updated_by']);
    }

    public static function findTrainFromUser($userId)
    {
        return static::find()
		->where(['created_by'=>$userId])
		->one();
    }

}
