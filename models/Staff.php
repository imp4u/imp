<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "staff".
 *
 * @property integer $id
 * @property string $name
 * @property integer $staff_type
 * @property integer $department
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_type', 'department'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'שם מלא',
            'staff_type' => 'תפקיד',
            'department' => 'מחלקה',
        ];
    }
    public static function getStaffMembers()
	{
		$staffMemebers = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $staffMemebers;						
	}

    public function getStaffDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
    public function getStaffType()
    {
        return $this->hasOne(StaffType::className(), ['id' => 'staff_type']);
    }
}
