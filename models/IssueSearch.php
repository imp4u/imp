<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Issue;

/**
 * IssueSearch represents the model behind the search form about `app\models\Issue`.
 */
class IssueSearch extends Issue
{
    /**
     * @inheritdoc
     */
    public $date_range;

    public function rules()
    {
        return [
            [['id', 'date', 'created_by', 'admission_id','department', 'status', 'assign_to'], 'integer'],
            [['shift', 'staff_name', 'description', 'fix_description', 'notes','date_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Issue::find();
        $query -> joinWith('issueStaff');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->created_by == -1 ? $this->created_by = null : $this->created_by;
        $this->status == -1 ? $this->status = null : $this->status;
        $this->department == -1 ? $this->department = null : $this->department; 
        if(!empty($_GET['date_range'])){
        $this->date_range = $_GET['date_range'];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'issue.id' => $this->id,
            'created_by' => $this->created_by,
            'admission_id' => $this->admission_id,
            'status' => $this->status,
            'issue.department' => $this->department,
            'assign_to' => $this->assign_to,
        ]);

        $query->andFilterWhere(['like', 'shift', $this->shift])
            ->andFilterWhere(['like', 'staff.name', $this->staff_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'fix_description', $this->fix_description])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        if(!empty($this->date_range) && strpos($this->date_range, 'to') !== false) {
			list($start_date, $end_date) = explode('to', $this->date_range);
			$query->andFilterWhere(['between', 'date', strtotime($start_date), strtotime($end_date)]);
		}		

        return $dataProvider;
    }
}
