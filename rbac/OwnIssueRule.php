<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnIssueRule extends Rule
{
	public $name = 'ownIssueRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['issue']) ? $params['issue']->created_by == $user : false;
            //$params['issue'] is an object. $params['issue']->created_by is the id of the user who created the issue
		}
		return false;
	}
}