<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnTrainingRule extends Rule
{
	public $name = 'ownTrainingRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['training']) ? $params['training']->created_by == $user : false;
            //$params['training'] is an object. $params['training']->created_by is the id of the user who created the training
		}
		return false;
	}
}