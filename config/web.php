<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    // set target language to be Russian
    'language' => 'he',
    
    // set source language to be English
    'sourceLanguage' => 'en-US',
    'modules' => [
        'datecontrol' =>  [
                'class' => '\kartik\datecontrol\Module',
            ],

        'gridview' =>  [
                'class' => '\kartik\grid\Module',
            ],
        
    ],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1MRskMhOSwsTA3C_nWQCi3lZUL4eLrTn',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        /*'view' => [
                'theme' => [
                    'pathMap' => ['@app/views' => '@app/themes/tf-dorian'],
                    'baseUrl' => '@web/../themes/tf-dorian',
                ],
            ],*/

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'users' => 'user/index',
                'user/<id:\d+>' => 'user/view',
                'issues' => 'issue/index',
                'issue/<id:\d+>' => 'issue/view',
                'dept' => 'department/index',
                'dept/<id:\d+>' => 'dept/view',
                'statuses' => 'status/view',
                'status/<id:\d+>' => 'status/view',
                'staff' => 'staff/index',
                'staff/<id:\d+>' => 'staff/view',
                'training' => 'training/index',
                'training/<id:\d+>' => 'training/view',
                'staff-type' => 'staff-type/index',
                'staff-type/<id:\d+>' => 'staff-type/view',
                'training-type' => 'training-type/index',
                'training-type/<id:\d+>' => 'training-type/view',
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
